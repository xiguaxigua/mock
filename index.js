const Koa = require('koa');
const Router = require('koa-router');
const cors = require('@koa/cors');
const fs = require('fs');
const path = require('path');
const routes = require('./routes');

const app = new Koa();
const router = new Router();

app.use(cors());
app.use(router.routes()).use(router.allowedMethods());

routes.forEach(({ url, method = 'get', res }) => {
  router[method](url, async (ctx, next) => {
    const data = fs.readFileSync(
      path.resolve(__dirname, `./data/${res}.json`),
      'UTF-8'
    );
    ctx.body = data;
    await next();
  });
});

app.listen(9527);
