# 开始使用

- `git clone git@gitlab.com:xiguaxigua/mock.git` 拉仓库
- `npm i` 安装依赖
- `npm start` 启动服务器
- 打开浏览器, 访问 <http://localhost:9527/> 中的对应路径


# 增加新的数据

- 在data目录下增加 {name}.json
- 在routes.js文件中增加映射 `{ url: '{path}', res: '{name}' }`


# 备注

## 模拟post请求

`{ url: '{path}', res: '{name}', method: 'post' }`
